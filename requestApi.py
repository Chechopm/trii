import requests

urlBase = 'https://financialmodelingprep.com/api/v3/quote-short/'
apiKey = '?apikey=c13a5d2ecf7cc6b8c50c06d7e1dfce22'


def callStockInfo(stock):
    url = urlBase + stock + 'GOOGL'
    res = requests.get(url)
    return res.text
